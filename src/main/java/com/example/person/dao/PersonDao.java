package com.example.person.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.person.dab.PersonDab;

public interface PersonDao extends JpaRepository<PersonDab, Integer> {

}
