package com.example.person.controller.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonDto {
	private Integer personId;
	private String firstName;
	private String lastName;
	private LocalDateTime dateOfBirth;
	private String gender;
}
