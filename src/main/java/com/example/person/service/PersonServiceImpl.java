package com.example.person.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.person.controller.dto.PersonDto;
import com.example.person.entity.Person;
import com.example.person.repository.PersonRepository;

@Service
public class PersonServiceImpl implements PersonService {

	@Autowired
	private PersonRepository repository;

	@Override
	public List<PersonDto> getAllPersons() {
		return repository.getAllPersons().stream().map(person -> {
			PersonDto dto = new PersonDto();
			BeanUtils.copyProperties(person, dto);
			return dto;
		}).collect(Collectors.toList());
	}

	@Override
	public PersonDto getPerson(Integer personId) {
		Person person = repository.getPerson(personId);
		PersonDto dto = new PersonDto();
		BeanUtils.copyProperties(person, dto);
		return dto;
	}

	@Override
	public Integer savePerson(PersonDto personDto) {
		Person person = Person.builder().personId(personDto.getPersonId())
		.firstName(personDto.getFirstName())
				.lastName(personDto.getLastName())
				.dateOfBirth(personDto.getDateOfBirth())
				.gender(personDto.getGender())
				.build();
		return repository.savePerson(person);
	}

	@Override
	public void updatePerson(PersonDto person) {
		// TBD
	}

	@Override
	public void deletePerson(Integer personId) {
		repository.deletePerson(personId);
	}
	
}
