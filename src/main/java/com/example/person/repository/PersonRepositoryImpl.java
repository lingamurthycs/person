package com.example.person.repository;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.person.dab.PersonDab;
import com.example.person.dao.PersonDao;
import com.example.person.entity.Person;

@Repository
public class PersonRepositoryImpl implements PersonRepository {

	@Autowired
	private PersonDao personDao;

	@Override
	public List<Person> getAllPersons() {
		return personDao.findAll().stream()
				.map(dab -> buildPersonEntityFromDab(dab))
				.collect(Collectors.toList());
	}

	@Override
	public Person getPerson(Integer personId) {
		return buildPersonEntityFromDab(personDao.getOne(personId));
	}

	@Override
	public Integer savePerson(Person person) {
		PersonDab dab = personDao.save(buildPersonDabFromEntity(person));
		return dab.getPersonId();
	}
	
	@Override
	public void deletePerson(Integer personId) {
		personDao.deleteById(personId);
	}
	
	private Person buildPersonEntityFromDab(PersonDab dab) {
		return Person.builder()
				.personId(dab.getPersonId())
				.firstName(dab.getFirstName())
				.lastName(dab.getLastName())
				.dateOfBirth(dab.getDateOfBirth())
				.gender(dab.getGender())
				.build();
	}
	
	private PersonDab buildPersonDabFromEntity(Person person) {
		PersonDab dab = new PersonDab();
		dab.setPersonId(person.getPersonId());
		dab.setFirstName(person.getFirstName());
		dab.setLastName(person.getLastName());
		dab.setDateOfBirth(person.getDateOfBirth());
		dab.setGender(person.getGender());
		return dab;
	}

}
