package com.example.person.service;

import java.util.List;

import com.example.person.controller.dto.PersonDto;

public interface PersonService {
	List<PersonDto> getAllPersons();

	PersonDto getPerson(Integer personId);

	Integer savePerson(PersonDto person);

	void updatePerson(PersonDto person);

	void deletePerson(Integer personId);
}
