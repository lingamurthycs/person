package com.example.person.repository;

import java.util.List;

import com.example.person.entity.Person;

public interface PersonRepository {

	List<Person> getAllPersons();

	Person getPerson(Integer personId);

	Integer savePerson(Person person);

	// void updatePerson(Person person);

	void deletePerson(Integer personId);

}
