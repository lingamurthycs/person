package com.example.person.controller;

import javax.websocket.server.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.person.controller.dto.DeletePersonRequest;
import com.example.person.controller.dto.GetAllPersonsResponse;
import com.example.person.controller.dto.PersonDto;
import com.example.person.controller.dto.SavePersonResponse;
import com.example.person.service.PersonService;

@RestController
@RequestMapping("/person")
public class PersonController {
	Logger logger = LoggerFactory.getLogger(PersonController.class);

	@Autowired
	private PersonService personService;

	@GetMapping("/getall")
	@ResponseStatus(HttpStatus.OK)
	public GetAllPersonsResponse getAllPersons() {
		logger.debug("getall request received.");
		GetAllPersonsResponse response = new GetAllPersonsResponse();
		response.setPersons(personService.getAllPersons());
		return response;
	}

	@GetMapping("/get/{personid}")
	@ResponseStatus(HttpStatus.OK)
	public PersonDto getPerson(@PathParam("personid") Integer personId) {
		logger.debug("get id received {}", personId);
		return personService.getPerson(personId);
	}

	@PostMapping("/create")
	@ResponseStatus(HttpStatus.CREATED)
	public SavePersonResponse savePerson(@RequestBody PersonDto request) {
		SavePersonResponse response = new SavePersonResponse();
		response.setPersonId(personService.savePerson(request));
		return response;
	}

	@PutMapping("/update")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Void> updatePerson(@RequestBody PersonDto request) {
		personService.updatePerson(request);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@DeleteMapping("/delete")
	@ResponseStatus(HttpStatus.OK)
	public void deletePerson(@RequestBody DeletePersonRequest request) {
		personService.deletePerson(request.getPersonid());
	}

}
