package com.example.person.controller.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SavePersonResponse {
	private Integer personId;
}
