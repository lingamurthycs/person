package com.example.person;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.person.controller.dto.PersonDto;
import com.example.person.entity.Person;
import com.example.person.repository.PersonRepository;
import com.example.person.service.PersonServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonServiceImplTest {

	@InjectMocks
	private PersonServiceImpl service;

	@Mock
	private PersonRepository repository;

	List<Person> personList;

	@Before
	public void setup() {
		personList = Arrays.asList(TestUtil.buildPerson());
		// given
		when(repository.getAllPersons()).thenReturn(personList);
	}

	@Test
	public void getAllPersonsReturnsSuccessfully() {
		// when
		List<PersonDto> personDtos = service.getAllPersons();

		// then
		assertNotNull(personDtos);
		assertFalse(personDtos.isEmpty());
		assertEquals(personList.size(), personDtos.size());
	}

	@Test
	public void getAllPersonsInvokesRepository() {
		// when
		service.getAllPersons();

		// then
		verify(repository).getAllPersons();

	}
}
