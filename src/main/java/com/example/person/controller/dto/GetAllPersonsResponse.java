package com.example.person.controller.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetAllPersonsResponse {
	private List<PersonDto> persons;
}
