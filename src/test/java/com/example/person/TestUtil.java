package com.example.person;

import java.time.LocalDateTime;

import com.example.person.entity.Person;

public class TestUtil {

	public static final String FIRST_NAME = "FIRST_NAME";
	public static final String LAST_NAME = "LAST_NAME";
	public static final LocalDateTime DATE_OF_BIRTH = LocalDateTime.now();
	public static final String GENDER = "M";

	public static Person buildPerson() {
		return Person.builder()
		.personId(1)
		.firstName(FIRST_NAME)
		.lastName(LAST_NAME)
		.dateOfBirth(DATE_OF_BIRTH)
		.gender(GENDER).build();
	}
}
